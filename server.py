"""
This template is heavily inspired by idcard authentication project.
So, the name of main model is IDcardAuth and it is expected to be located at idcard_auth.py
"""

import logging
import argparse
import grpc
import time
import torch

from concurrent import futures

from idcard_auth import IDcardAuth  # Please specify your own model
from brain_vision_example_pb2_grpc import (
    add_MyServiceServicer_to_server,
    MyServiceServicer,
)
from brain_vision_example_pb2 import MyResult, Patch, REAL, MONITOR, PAPER

_ONE_DAY_IN_SECONDS = 60 * 60 * 24


class MyServiceServicerImpl(MyServiceServicer):
    def __init__(self, model, gpuid):
        super().__init__()
        self.my_model = IDcardAuth(model, gpuid)  # call your model
        self.label_dict = {  # The enumerator is declared when setting up .proto. Check out brain-vision-example.proto!
            "monitor": MONITOR,
            "paper": PAPER,
            "real": REAL,
        }

    def Determinate(self, my_input_iterator, context):  # mainly called from client
        try:
            torch.cuda.set_device(self.my_model.device)

            # The format of iterator can be found in client.py.
            # it splits a binary image into chunk_size and yields them.
            img_data = bytearray()
            for id_card in my_input_iterator:
                img_data.extend(id_card.data)
            img_data = bytes(img_data)  # server: received binary image
            logging.debug("request:%s byte", len(img_data))

            # Model inference
            # check out your function name in your own model
            # it may not be 'determination'
            max_pred, patches = self.my_model.determination(img_data)
            logging.debug("label count:%s", max_pred)
            logging.debug("patches:%s", patches)

            # (Optional) Post-process for structured data with output
            patch_list = []
            for patch in patches:
                patch_width = patch.w_max - patch.w_min
                patch_height = patch.h_max - patch.h_min

                patch = Patch(
                    label=self.label_dict[patch.name],
                    width=patch_width,
                    height=patch_height,
                    left=patch.w_min,
                    top=patch.h_min,
                )
                patch_list.append(patch)

            max_pred = max(max_pred, key=max_pred.get)
            label = self.label_dict[max_pred]
            logging.debug("label:%s", label)

            return MyResult(label=label, patches=patch_list)

        except Exception as e:  # logging when an exception occurs
            logging.exception(e)
            context.set_code(grpc.StatusCode.UNKNOWN)
            context.set_details(str(e))


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--model",
        type=str,
        help="name of the model which is used for test",
        required=True,
    )
    parser.add_argument("--gpuid", type=str, default="0", help="gpu id")
    parser.add_argument("--port", type=int, default=11000, help="grpc port")
    parser.add_argument("--log_level", type=str, default="INFO", help="logger level")
    args = parser.parse_args()

    logging.basicConfig(
        level=getattr(logging, args.log_level),
        format="[%(levelname)s|%(filename)s:%(lineno)s][%(asctime)s] >>> %(message)s",
    )

    server = grpc.server(
        futures.ThreadPoolExecutor(max_workers=1),
    )
    idcard_auth = MyServiceServicerImpl(args.model, args.gpuid)
    add_MyServiceServicer_to_server(idcard_auth, server)
    server.add_insecure_port("[::]:{}".format(args.port))
    server.start()

    logging.info("MyService starting at 0.0.0.0:%d", args.port)
    try:
        while True:
            # Sleep forever, since `start` doesn't block
            time.sleep(_ONE_DAY_IN_SECONDS)
    except KeyboardInterrupt:
        server.stop(0)
