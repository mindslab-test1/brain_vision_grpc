# brain-vision-grpc

본 문서는 MINDsLab brain-vision 팀에서 자주 사용하는 In/Output에 대한 grpc 패키징을 일원화 하는 데에 주 목적이 있습니다.
아래는 앞으로 진행해야 할 사항과 본 Repository에 반영된 사항들 입니다.


### Note: git hook in pre-commit

![https://github.com/psf/black](https://img.shields.io/badge/code%20style-black-000000.svg)

본 repository는 python code formatter 인 black이 git hook으로 작동하도록 구성하고 있습니다.
pre-commit 시 python auto-formatting을 이용하실 분은 [black repo](https://github.com/psf/black) 및 본 [블로그](https://bmh8993.github.io/Python/auto-formatting-pep8-with-black-plus-git-hooks/)를 확인하시기 바랍니다.

## To-do
- [ ] 다른 Input에 대한 Example .proto
- [ ] Async 처

`2020. 09. 03`
- [x] Hello, brain-vision
- [x] make a proto template
- [x] add a comment in client.py
- [x] add a comment in server.py
- [x] test with an example model

## Usage

### Setup
```
python -m grpc.tools.protoc --proto_path=. --python_out=. --grpc_python_out=. id_card_authentication.proto
```

### Run Server
```
python server.py --model MODEL_PATH --gpuid GPUID --port PORT --log_level LOG_LEVEL, MODEL_PATH 필수
```

### Run Client
```
python client.py -r GRPC_IP_PORT -i IMG_PATH, IMG_PATH 필수
```

### test example model (idcard_auth.py)
IDcard Authentication에 대한 test_image와 weight file 은 김진우 연구원의 Google Drive에서 확인할 수 있습니다.  

[김진우 연구원의 Google Drive Link](https://drive.google.com/drive/folders/1wyvXQ4RfoWwaC0HCn5DMfKn7xi-bsb4C?usp=sharing)

pip dependency는 [`requirements_idcard_auth.txt`](./requirements_idcard_auth.txt)를 참고하세요.

### References
- https://grpc.io/docs/languages/python/basics/
- [용준영 연구님의 포스트](https://pms.maum.ai/confluence/x/RCeqAQ)

### Contributors
- myunchul
- acasia
- deepkyu

### Acknowledgement
본 template은 idcard authentication에 대해 조면철 전무님이 진행해주신 grpc packaging을 기반으로 시작합니다.  
`.obj`, `.mp4`, `.stl` 등 여러 형태의 In/Output 에 대해 예시를 추가하도록 하겠습니다.