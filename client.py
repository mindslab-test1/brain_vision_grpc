import grpc
import argparse

from brain_vision_example_pb2_grpc import MyServiceStub
from brain_vision_example_pb2 import MyInput


class MyServiceClient(object):
    def __init__(
        self, remote="127.0.0.1:11000", chunk_size=1024
    ):  # remote: same with localhost:11000
        channel = grpc.insecure_channel(remote)
        self.stub = MyServiceStub(channel)
        self.chunk_size = chunk_size

    def determinate(self, binary):
        binary_iterator = self._generate_binary_iterator(binary)
        return self.stub.Determinate(binary_iterator)

    def _generate_binary_iterator(self, binary):
        for idx in range(0, len(binary), self.chunk_size):
            yield MyInput(data=binary[idx : idx + self.chunk_size])


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="id card authentication client")
    parser.add_argument(
        "-r",
        "--remote",
        nargs="?",
        dest="remote",
        help="grpc: ip:port",
        type=str,
        default="127.0.0.1:11000",
    )  # same with 'localhost:11000'
    parser.add_argument(
        "-i",
        "--input",
        nargs="?",
        dest="input",
        help="image path",
        type=str,
        required=True,
    )
    args = parser.parse_args()

    client = MyServiceClient(args.remote)

    with open(args.input, "rb") as rf:
        data = rf.read()

    result = client.determinate(
        data
    )  # return in MyServiceServicerImpl.Determinate server.py
    print(result)  # will be shown in shell
