# -*- coding: utf-8 -*-
from __future__ import print_function, division
import collections
import numpy as np
import argparse
import torch
from torchvision import transforms
import cv2
from PIL import Image
import torch.nn.functional as F


Patch = collections.namedtuple("patch", ["name", "h_min", "h_max", "w_min", "w_max"])


class IDcardAuth:
    def __init__(self, model, gpuid):
        self.model = None
        self.checkpoint_path = model
        self.device = torch.device(
            "cuda:%s" % gpuid if torch.cuda.is_available() else "cpu"
        )
        print("using ", self.device)
        self.load_model()
        self.data_transforms = transforms.Compose(
            [
                transforms.Resize((224, 224)),
                transforms.ToTensor(),
                transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225]),
            ]
        )

    def load_model(self):
        self.model = torch.load(self.checkpoint_path)
        self.model.to(self.device)

    def determination(
        self, img_data
    ):  # it would be mainly used for a service in server.py
        # (Recommend) Decode a binary image into a uint8 image
        img = cv2.imdecode(np.fromstring(img_data, dtype=np.uint8), cv2.IMREAD_COLOR)

        """
        NOTE: If you use an img in the above, it would be BGR format which follows opencv legacy.
        In most cases, your model would be trained with RGB images,
        especially when you use pillow(PIL) or torchvision(dependent on pillow).
        To convert this, uncomment those lines before putting into your model
        """
        # image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
        # image = Image.fromarray(image)

        """
        Here in the below is mostly about an example service.
        It would correspond to inference your own model.
        Please note that it is not relevant to grpc packaging
        The author of the sample model is acasia (acasia@mindslab.ai)
        """
        h, w, c = img.shape

        h_range = np.linspace(0, h, 4, dtype=int)  # if h == 3, [0, 1, 2, 3]
        w_range = np.linspace(0, w, 5, dtype=int)  # if w == 4, [0, 1, 2, 3, 4]

        max_pred = {"monitor": 0, "paper": 0, "real": 0}
        patch_pred = {
            "patch_0": None,
            "patch_1": None,
            "patch_2": None,
            "patch_3": None,
            "patch_4": None,
            "patch_5": None,
            "patch_6": None,
            "patch_7": None,
            "patch_8": None,
            "patch_9": None,
            "patch_10": None,
            "patch_11": None,
        }

        # make image 12 patches
        patch_idx = 0
        patches = []
        for patch_w_idx in range(len(w_range) - 1):
            for patch_h_idx in range(len(h_range) - 1):

                # define patch area
                h_min, h_max = h_range[patch_h_idx], h_range[patch_h_idx + 1]
                w_min, w_max = w_range[patch_w_idx], w_range[patch_w_idx + 1]

                # get image patch
                image = img[h_min:h_max, w_min:w_max]

                # image patch convert to model input format
                image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
                image = Image.fromarray(image)
                inputs = self.data_transforms(image).float()
                inputs = inputs[None, :, :]

                with torch.no_grad():
                    self.model.eval()
                    outputs = F.softmax(self.model(inputs.cuda()), dim=1)
                    _, preds = torch.max(outputs, 1)

                    preds_name = ["monitor", "paper", "real"][int(preds.item())]
                    max_pred[preds_name] += 1
                    patch_pred["patch_" + str(patch_idx)] = preds_name
                patch_idx += 1

                patches.append(Patch(preds_name, h_min, h_max, w_min, w_max))

        return max_pred, patches


def apply_img_color(cv2_img, preds_name, h_min, h_max, w_min, w_max):
    if preds_name == "monitor":
        # apply red color
        cv2_img[h_min:h_max, w_min:w_max][:, :, 0] = 0
        cv2_img[h_min:h_max, w_min:w_max][:, :, 1] = 0
    elif preds_name == "paper":
        # apply blue color
        cv2_img[h_min:h_max, w_min:w_max][:, :, 1] = 0
        cv2_img[h_min:h_max, w_min:w_max][:, :, 2] = 0


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--model",
        type=str,
        default="./weight/idcard_auth.pth",
        help="name of the model which is used for test",
    )
    parser.add_argument("--input", type=str, required=True, help="input image path")
    parser.add_argument("--gpuid", type=str, default="0", help="gpu id")
    args = parser.parse_args()

    idcard_auth = IDcardAuth(args.model, args.gpuid)
    with open(args.input, "rb") as rf:
        data = rf.read()
    final_pred, final_patches = idcard_auth.determination(data)
    print(final_pred)

    final_img = cv2.imdecode(np.fromstring(data, dtype=np.uint8), cv2.IMREAD_COLOR)
    for patch in final_patches:
        apply_img_color(
            final_img, patch.name, patch.h_min, patch.h_max, patch.w_min, patch.w_max
        )
    cv2.imwrite("pred_result.jpg", final_img)
    print("saved result image")

    final_pred = max(final_pred, key=final_pred.get)
    print(final_pred)
